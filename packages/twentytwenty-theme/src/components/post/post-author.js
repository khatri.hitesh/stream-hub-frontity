import { styled, connect } from "frontity";
import React from "react";
import Link from "../link";
import Image from "@frontity/components/image";
import facebookImage from "../../../../../assets/facebook.png";
import linkedinImage from "../../../../../assets/linkedin.png";
import arrowImage from "../../../../../assets/arrow.png";
import personImage from "../../../../../assets/person.png";
const PostAuthor = ({ state, item }) => {
  const author = state.source.author[item.author];

  return (
    <PostInner size="thin">
      <EntryContent>
        <div className="author-title">
          <div className="author-title">
            <Image
              className="autor-img"
              alt={author.name}
              width="45"
              height="45"
              src={author.avatar_urls['48']}
              title={author.name} >
            </Image>
            <div className="author-title-info">
              <Link link={author.link}>
                {
                  author.name
                }
              </Link>
              <p>@{author.name}
              </p>
            </div>
          </div>
          <div className="author-link-btn">
            <a href="https://www.facebook.com/concalahan" target="_blank" rel="noopener noreferrer"><img src={facebookImage} /></a>
            <a href="https://www.linkedin.com/in/concalahan/" target="_blank" rel="noopener noreferrer"><img src={linkedinImage} /></a>
          </div>
        </div>
        <p>
          {
            author.description
          }
        </p>

        <div className="see-more">
          {/* <div className="see-more-link">
            <img src={personImage} /> <Link link={author.link}>Xem thêm</Link>
          </div> */}
          <Link link={author.link}><img src={arrowImage} /></Link>
        </div>

      </EntryContent>
    </PostInner>
  )
}

export default connect(PostAuthor);

// Header sizes bases on style.css
const maxWidths = {
  thin: "58rem",
  small: "80rem",
  medium: "100rem"
};

const getMaxWidth = props => maxWidths[props.size] || maxWidths["medium"];

export const SectionContainer = styled.div`
  border: 1px solid #e1e8ed;
  `;

export const PostInner = styled(SectionContainer)`
  letter-spacing: normal;
  font-size: 1em;
  color: #333;
  font-family: Inter;
  border-radius: 6px;
  transition: all 0.15s ease;
  padding: 20px;
  background-color: #fff;
  margin-bottom: 25px;
`;

export const EntryContent = styled.div`
  line-height: 1.5;
  font-family: Inter;
  letter-spacing: normal;

  @media (min-width: 700px) {
    font-size: 2.1rem;
  }

  figure {
    margin: 2em 0;
    max-width: 100%;
  }

  p {
    margin-bottom: 0px;
    color: #1C2022;
  }
`;